﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace Kurucular
{
    class Araba
    {
        private System.Windows.Forms.Timer _zamanlayici;

        private int _hiz;
        public int Hiz
        {
            get { return _hiz; }
            set
            {
                if (value >=0 && value <= 200)
                {
                    _hiz = value;
                }
            }
        }

        private int _benzinMiktari;

        public int BenzinMiktari
        {
            get { return _benzinMiktari; }
            set
            {
                if (value >=0 && value <= 5000)
                {
                    _benzinMiktari = value;
                }
            }
        }

        private int _motorSicakligi;

        public int MotorSicakligi
        {
            get { return _motorSicakligi; }
            set
            {
                if (value >=0 && value <= 100)
                {
                    _motorSicakligi = value;
                }
            }
        }

        public Araba() : this(0, 5000, 0)
        {
        }

        private void ZamanGecti(object sender, EventArgs e)
        {
            BenziniAzalt();
            MotorSicakliginiArttir();
        }

        private void MotorSicakliginiArttir()
        {
            if (MotorSicakligi < 50) MotorSicakligi++;
        }

        private void BenziniAzalt()
        {
            var azalmaMiktari = Hiz;
            BenzinMiktari -= azalmaMiktari;
        }

        public Araba(int hiz, int benzinMiktari, int motorSicakligi)
        {
            Hiz = hiz;
            BenzinMiktari = benzinMiktari;
            MotorSicakligi = motorSicakligi;

            _zamanlayici = new System.Windows.Forms.Timer();
            _zamanlayici.Interval = 1000;
            _zamanlayici.Enabled = true;

            _zamanlayici.Tick += ZamanGecti;
        }
    }
}
