﻿namespace Kurucular
{
    partial class AnaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AnaForm));
            this.frenButton = new System.Windows.Forms.Button();
            this.gazButton = new System.Windows.Forms.Button();
            this.baslatButton = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.hizPictureBox = new System.Windows.Forms.PictureBox();
            this.benzinPictureBox = new System.Windows.Forms.PictureBox();
            this.motorSicakligiPictureBox = new System.Windows.Forms.PictureBox();
            this.motorSicakligiProgressBar = new System.Windows.Forms.ProgressBar();
            this.benzinProgressBar = new System.Windows.Forms.ProgressBar();
            this.hizLabel = new System.Windows.Forms.Label();
            this.hizProgressBar = new System.Windows.Forms.ProgressBar();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.hizPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.benzinPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.motorSicakligiPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // frenButton
            // 
            this.frenButton.Location = new System.Drawing.Point(171, 223);
            this.frenButton.Name = "frenButton";
            this.frenButton.Size = new System.Drawing.Size(153, 37);
            this.frenButton.TabIndex = 0;
            this.frenButton.Text = "FREN";
            this.frenButton.UseVisualStyleBackColor = true;
            this.frenButton.Click += new System.EventHandler(this.frenButton_Click);
            // 
            // gazButton
            // 
            this.gazButton.Location = new System.Drawing.Point(330, 223);
            this.gazButton.Name = "gazButton";
            this.gazButton.Size = new System.Drawing.Size(153, 37);
            this.gazButton.TabIndex = 2;
            this.gazButton.Text = "GAZ";
            this.gazButton.UseVisualStyleBackColor = true;
            this.gazButton.Click += new System.EventHandler(this.gazButton_Click);
            // 
            // baslatButton
            // 
            this.baslatButton.Location = new System.Drawing.Point(12, 223);
            this.baslatButton.Name = "baslatButton";
            this.baslatButton.Size = new System.Drawing.Size(153, 37);
            this.baslatButton.TabIndex = 5;
            this.baslatButton.Text = "BAŞLAT";
            this.baslatButton.UseVisualStyleBackColor = true;
            this.baslatButton.Click += new System.EventHandler(this.baslatButton_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.hizPictureBox);
            this.panel1.Controls.Add(this.benzinPictureBox);
            this.panel1.Controls.Add(this.motorSicakligiPictureBox);
            this.panel1.Controls.Add(this.motorSicakligiProgressBar);
            this.panel1.Controls.Add(this.benzinProgressBar);
            this.panel1.Controls.Add(this.hizLabel);
            this.panel1.Controls.Add(this.hizProgressBar);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(472, 205);
            this.panel1.TabIndex = 10;
            // 
            // hizPictureBox
            // 
            this.hizPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("hizPictureBox.Image")));
            this.hizPictureBox.Location = new System.Drawing.Point(18, 103);
            this.hizPictureBox.Name = "hizPictureBox";
            this.hizPictureBox.Size = new System.Drawing.Size(40, 42);
            this.hizPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.hizPictureBox.TabIndex = 16;
            this.hizPictureBox.TabStop = false;
            // 
            // benzinPictureBox
            // 
            this.benzinPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("benzinPictureBox.Image")));
            this.benzinPictureBox.Location = new System.Drawing.Point(18, 59);
            this.benzinPictureBox.Name = "benzinPictureBox";
            this.benzinPictureBox.Size = new System.Drawing.Size(40, 38);
            this.benzinPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.benzinPictureBox.TabIndex = 15;
            this.benzinPictureBox.TabStop = false;
            // 
            // motorSicakligiPictureBox
            // 
            this.motorSicakligiPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("motorSicakligiPictureBox.Image")));
            this.motorSicakligiPictureBox.Location = new System.Drawing.Point(18, 15);
            this.motorSicakligiPictureBox.Name = "motorSicakligiPictureBox";
            this.motorSicakligiPictureBox.Size = new System.Drawing.Size(40, 38);
            this.motorSicakligiPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.motorSicakligiPictureBox.TabIndex = 14;
            this.motorSicakligiPictureBox.TabStop = false;
            // 
            // motorSicakligiProgressBar
            // 
            this.motorSicakligiProgressBar.Location = new System.Drawing.Point(64, 15);
            this.motorSicakligiProgressBar.Name = "motorSicakligiProgressBar";
            this.motorSicakligiProgressBar.Size = new System.Drawing.Size(390, 38);
            this.motorSicakligiProgressBar.TabIndex = 13;
            // 
            // benzinProgressBar
            // 
            this.benzinProgressBar.Location = new System.Drawing.Point(64, 59);
            this.benzinProgressBar.Maximum = 5000;
            this.benzinProgressBar.Name = "benzinProgressBar";
            this.benzinProgressBar.Size = new System.Drawing.Size(390, 38);
            this.benzinProgressBar.TabIndex = 12;
            // 
            // hizLabel
            // 
            this.hizLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.hizLabel.Location = new System.Drawing.Point(64, 148);
            this.hizLabel.Name = "hizLabel";
            this.hizLabel.Size = new System.Drawing.Size(390, 35);
            this.hizLabel.TabIndex = 11;
            this.hizLabel.Text = "0 km/h";
            this.hizLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // hizProgressBar
            // 
            this.hizProgressBar.Location = new System.Drawing.Point(64, 103);
            this.hizProgressBar.Maximum = 200;
            this.hizProgressBar.Name = "hizProgressBar";
            this.hizProgressBar.Size = new System.Drawing.Size(390, 42);
            this.hizProgressBar.TabIndex = 10;
            // 
            // AnaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(499, 273);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.baslatButton);
            this.Controls.Add(this.gazButton);
            this.Controls.Add(this.frenButton);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(6);
            this.MaximizeBox = false;
            this.Name = "AnaForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ARABA SİMÜLATÖRÜ";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.hizPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.benzinPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.motorSicakligiPictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button frenButton;
        private System.Windows.Forms.Button gazButton;
        private System.Windows.Forms.Button baslatButton;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox hizPictureBox;
        private System.Windows.Forms.PictureBox benzinPictureBox;
        private System.Windows.Forms.PictureBox motorSicakligiPictureBox;
        private System.Windows.Forms.ProgressBar motorSicakligiProgressBar;
        private System.Windows.Forms.ProgressBar benzinProgressBar;
        private System.Windows.Forms.Label hizLabel;
        private System.Windows.Forms.ProgressBar hizProgressBar;
    }
}

