﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kurucular
{
    public partial class AnaForm : Form
    {
        private Araba _honda;

        public AnaForm()
        {
            InitializeComponent();
        }



        private void frenButton_Click(object sender, EventArgs e)
        {
            _honda.Hiz -= 10;

            GostergePaneliniGuncelle();
        }

        private void GostergePaneliniGuncelle()
        {
            hizProgressBar.Value = _honda.Hiz;
            hizLabel.Text = _honda.Hiz.ToString();
            benzinProgressBar.Value = _honda.BenzinMiktari;
            motorSicakligiProgressBar.Value = _honda.MotorSicakligi;
        }

        private void gazButton_Click(object sender, EventArgs e)
        {
            _honda.Hiz += 10;

            GostergePaneliniGuncelle();
        }

        private void baslatButton_Click(object sender, EventArgs e)
        {
            var sp = new SoundPlayer("start.wav");
            sp.Play();

            _honda = new Araba();

            timer1.Enabled = true;
            GostergePaneliniGuncelle();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            GostergePaneliniGuncelle();
        }
    }
}
